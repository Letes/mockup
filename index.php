<? session_start();
$fp = fopen("config/conf.txt", "r");
while(!feof($fp)) {
$linea = $linea.fgets($fp);
}
fclose($fp);	
$datos = explode(": ", $linea);

$_SESSION[URL_DB]=  substr($datos[1],0,-7); 
$_SESSION[DB_Port]=  substr($datos[2],0,-7);
$_SESSION[DB_Name]= substr($datos[3],0,-7); 
$_SESSION[DB_User]= substr($datos[4],0,-13); 
$_SESSION[DB_Password]= substr($datos[5],0,-13); 
$_SESSION[URL_Operation]=$datos[6]; 

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Documento sin título</title>
<style type="text/css">
#contenido {
	position: absolute;
	width: 963px;
	height: 523px;
	z-index: 1;
	top: 37px;
}
#apDiv1 {
	position: absolute;
	width: 200px;
	height: 23px;
	z-index: 2;
	left: 753px;
	top: 9px;
}
</style>
</head>

<body>
<div>
  <label for="operation">operation:</label>
  <select name="operation" id="operation" onchange="document.getElementById('content').src=this.value;">
    <option value="">Select an operation</option>
    <option value="operations/ParkPlaceInfoRequest.php">ParkPlaceInfoRequest</option>
    <option value="operations/CheckOut.php">CheckOut</option>
    <option value="operations/CheckDate.php">CheckDate</option>
    <option value="operations/CheckStartHour.php">CheckStartHour</option>
    <option value="operations/CheckDuration.php">CheckDuration</option>
    <option value="operations/Reserve.php">Reserve</option>
    <option value="operations/CurrentCost.php">CurrentCost</option>
    <option value="operations/ReportIncident.php">ReportIncident</option>
    <option value="operations/ListPayments.php">ListPayments</option>
    <option value="operations/ListReservations.php">ListReservations</option>
    <option value="operations/CheckAdjacentReservation.php">CheckAdjacentReservation</option>
    <option value="operations/ParkingHumanValidationUpdate.php">ParkingHumanValidationUpdate</option>
    <option value="operations/ParkingHumanValidationCancel.php">ParkingHumanValidationCancel</option>
    <option value="operations/ParkingHumanValidationAdd.php">ParkingHumanValidationAdd</option>
    <option value="operations/SimulateSensor.php">SimulateSensor</option>
    <option value="operations/FineCar.php">FineCar</option>
    <option value="operations/ListParkingClusters.php">ListParkingClusters</option>
    <option value="operations/AddUserRegistration.php">AddUserRegistration</option>
    <option value="operations/UpdateUserRegistration.php">UpdateUserRegistration</option>
    <option value="operations/ListOffers.php">ListOffers</option>
    <option value="operations/FlashOfferUpload.php">FlashOfferUpload</option>
    <option value="operations/CarPlateHistory.php">CarPlateHistory</option>
    <option value="operations/CheckIn.php">CheckIn</option>
    <option value="operations/CheckInBis.php">CheckInBis</option>
    <option value="operations/TopUP.php">TopUP</option>
    <option value="operations/getParkingByIDs.php">getParkingByID</option>
    <option value="operations/AddBackEndUser.php">AddBackEndUser</option>
    <option value="operations/LoginRetailerUser.php">LoginRetailerUser</option>
    <option value="operations/LoginBackEndUser.php">LoginBackEndUser</option>  
    <option value="operations/Help.php">Help</option>     ParkPlaceInfoRequestBis 
    <option value="operations/ParkPlaceInfoRequestBis.php">ParkPlaceInfoRequestBis</option>     
    <option value="operations/ParkPlaceInfoRequestForStewart.php">ParkPlaceInfoRequestForStewart</option>  
</select>
  <div id="apDiv1"><a href="config/config.php">Config</a></div>
</div>
 
<div id="contenido">
  <iframe id="content" src="" width="950" height="500"/>
</div>
</body>
</html>