<?  session_start(); 
$opti=$_GET[option];
switch ($opti) {
    case 0:
	$fn="CheckOut";	
	$jsonData = array(
			'P_ID' => $_POST[P_ID],
			'PS_ID' => $_POST[PS_ID],
			'RU_CarPlate' => $_POST[RU_CarPlate],
			'RU_Email' => $_POST[RU_Email],
			'RU_ID'=> $_POST[RU_ID],
			'RU_WifiMAC' => $_POST[RU_WifiMAC]
	);
        break;
    case 1:
	$fn="CheckDate";
	$jsonData = array(
			'Date' => $_POST[Date], //mandatory
			'DeploymentID' => $_POST[DeploymentID]
	);
        break;
    case 2:
	$fn="CheckStartHour";
	$jsonData = array(
			'Date' => $_POST[Date],   //mandatory
			'Hour' => $_POST[Hour],   //mandatory
			'DeploymentID'=> $_POST[DeploymentID]
	);
        break;

    case 3:
	$fn="CheckDuration";
	$jsonData = array(
			'Date' => $_POST[Date],    //mandatory
			'Hour' => $_POST[Hour],    //mandatory
			'Duration'=> $_POST[Duration], //mandatory
			'DeploymentID' => $_POST[DeploymentID]
	);
        break;

    case 4:
	$fn="Reserve";
	$jsonData = array(
			'P_ID' =>  $_POST[P_ID], //mandatory
			'RU_Email' =>  $_POST[RU_Email],   
			'RU_ID'=>  $_POST[RU_ID], 
			'RU_CarPlate' =>  $_POST[RU_CarPlate],
			'Date' =>  $_POST[Date], //mandatory
			'Hour' =>  $_POST[Hour],   //mandatory
			'Duration'=>  $_POST[Duration], //mandatory
			'DeploymentID' =>  $_POST[DeploymentID]		
	);
        break;

    case 5:
	$fn="CurrentCost";
	$jsonData = array(
			'P_ID' =>  $_POST[P_ID], 
			'PS_ID' =>  $_POST[PS_ID],   
			'RU_Email'=>  $_POST[RU_Email], 
			'RU_ID' =>  $_POST[RU_ID],
			'RU_CarPlate' =>  $_POST[RU_CarPlate] 
	);
        break;

    case 6:
	$fn="ReportIncident";
	$jsonData = array(
			'P_ID' =>  $_POST[P_ID] ,   //mandatory
			'RU_Email' => $_POST[RU_Email] ,   
			'RU_ID'=>  $_POST[RU_ID] , 
			'RU_CarPlate' =>  $_POST[RU_CarPlate] ,
			'DeploymentID' =>  $_POST[DeploymentID] 
	);
        break;

    case 7:
	$fn="ListPayments";
	$jsonData = array(
			'RU_Email' => $_POST[RU_Email] ,   
			'RU_ID' => $_POST[RU_ID] ,   
			'RU_CarPlate'=> $_POST[RU_CarPlate]   
	);
        break;

    case 8:
	$fn="ListReservations";
	$jsonData = array(
			'RU_Email' => $_POST[RU_Email] , 
			'RU_ID' => $_POST[RU_ID] ,   
			'RU_CarPlate'=> $_POST[RU_CarPlate]   
	);
        break;
		
    case 9:
	$fn="CheckAdjacentReservation";
	$jsonData = array(
			'StartDate' => $_POST[StartDate]   ,  
			'Duration' =>  $_POST[Duration]   ,   
			'P_ID'=>  $_POST[P_ID]    
	);
        break;

     case 10:
	$fn="ParkingHumanValidationUpdate";
	$jsonData = array(
			'PHVID' => $_POST[PHVID] ,  
			'Value' => $_POST[Value] ,   
			'Text'=> $_POST[Text]   
	);
        break;             

     case 11:
	$fn="ParkingHumanValidationCancel";
	$jsonData = array(
			'PHVID' => $_POST[PHVID],  
			'Text' => $_POST[Text]
	);
        break;  
		           
     case 12:
	$fn="ParkingHumanValidationAdd";
	$jsonData = array(
			'ParkingID' => $_POST[P_ID],  
			'Occupancy' => $_POST[Occupancy],   
			'Text'=> $_POST[Text],
			'TS'=> $_POST[TS]  
	);
        break;             
		           
     case 13:
	$fn="SimulateSensor";
	$jsonData = array(
			'ParkingID' => $_POST[P_ID],  
			'SensorID' =>  $_POST[SensorID],   
			'NodeID'=>  $_POST[NodeID],
			'occ'=>  $_POST[occ] 
	);
        break; 
     case 14:
	$fn="FineCar";
	$jsonData = array(
			'P_ID' =>  $_POST[P_ID],  
			'ST_UserName' =>  $_POST[ST_UserName],   
			'RU_CarPlate'=>  $_POST[RU_CarPlate]  
			//$DeploymentID = 1;
	);
        break; 
     case 15:
	$fn="ListParkingClusters";
	$jsonData = array(
			'DeploymentID' => $_POST[DeploymentID] ,  
			'Coordinates' => $_POST[Coordinates]  
	);
        break; 
     case 16:
	$fn="AddUserRegistration";
	$jsonData = array(
			'RU_Title' => $_POST[RU_Title],  
			'RU_FirstName' => $_POST[RU_FirstName],   
			'RU_LastName'=> $_POST[RU_LastName],
			'RU_CarPlates' => $_POST[RU_CarPlates],  
			'RU_Email' => $_POST[RU_Email],   
			'RU_Phone'=> $_POST[RU_Phone],
			'RU_Username' => $_POST[RU_Username],  
			'RU_Password' => $_POST[RU_Password],   
			'RU_WifiMAC'=> $_POST[RU_WifiMAC],
			'RU_BluetoothMAC' =>$_POST[RU_BluetoothMAC],  
			'RU_EquipmentIdentifier' => $_POST[RU_EquipmentIdentifier],   
			'RU_FlashOfferReception'=> $_POST[RU_FlashOfferReception],
			'RU_CompanyName' => $_POST[RU_CompanyName],  
			'RU_DeploymentID' => $_POST[RU_DeploymentID],   
			'RU_LanguageID'=> $_POST[RU_LanguageID]
	);
        break;
		 
     case 17:
	$fn="UpdateUserRegistration";
	$jsonData = array(
			'RU_ID' => $_POST[RU_ID],  
			'RU_Title' => $_POST[RU_Title],  
			'RU_FirstName' => $_POST[RU_FirstName],   
			'RU_LastName'=> $_POST[RU_LastName],
			'RU_CarPlates' => $_POST[RU_CarPlates],  
			'RU_Email' => $_POST[RU_Email],   
			'RU_Phone'=> $_POST[RU_Phone],
			'RU_Username' => $_POST[RU_Username],  
			'RU_Password' => $_POST[RU_Password],   
			'RU_WifiMAC'=> $_POST[RU_WifiMAC],
			'RU_BluetoothMAC' =>$_POST[RU_BluetoothMAC],  
			'RU_EquipmentIdentifier' => $_POST[RU_EquipmentIdentifier],   
			'RU_FlashOfferReception'=> $_POST[RU_FlashOfferReception],
			'RU_CompanyName' => $_POST[RU_CompanyName],  
			'RU_DeploymentID' => $_POST[RU_DeploymentID],   
			'RU_LanguageID'=> $_POST[RU_LanguageID]	);
        break; 
     case 18:
	$fn="ListOffers";
	$jsonData = array(
			'FO_DeploymentID' => $_POST[FO_DeploymentID]	
	);
        break;             
     case 19:
	$fn="FlashOfferUpload";
	$jsonData = array(
			'DeploymentID' => $_POST[DeploymentID]	,  
			'RetailerUserID' => $_POST[RetailerUserID]	,   
			'StartDate'=> $_POST[StartDate]	,
			'EndDate' => $_POST[EndDate]	,  
			'Text' => $_POST[Text]	, 
			'FileName' => $_POST[FileName]	,  
			'Image'=>  $_POST[Image]	
	);
        break; 
     case 20:
	$fn="CarPlateHistory";
	$jsonData = array(
			'RU_CarPlate' =>$_POST[RU_CarPlate],  
			'ParkingID' => $_POST[ParkingID],   
			'Date'=> $_POST[Date]
	);
        break; 

     case 21:
	$fn="CheckIn";
	$jsonData = array(
			'P_ID' => $_POST[P_ID],  
			'PS_ID' => $_POST[PS_ID],   
			'RU_CarPlate'=> $_POST[RU_CarPlate],
			'RU_Email' => $_POST[RU_Email],  
			'RU_ID' => $_POST[RU_ID],   
			'RU_WifiMAC'=> $_POST[RU_WifiMAC],   
			'DeploymentID'=> $_POST[DeploymentID]
	);

        break; 

     case 22:
	$fn="CheckInBis";
	$jsonData = array(
			'P_ID' => $_POST[P_ID],  
			'RU_CarPlate' => $_POST[RU_CarPlate],   
			'DeploymentID'=> $_POST[DeploymentID],
			'PS_ID' => $_POST[PS_ID],  
			'RU_Email' => $_POST[RU_Email],   
			'RU_WifiMAC'=> $_POST[RU_WifiMAC]
	);
        break; 		
     case 23:
	$fn="TopUP";
	$jsonData = array(
			'RetailerUserID' => $_POST[RetailerUserID],  
			'ParkingID' => $_POST[ParkingID],   
			'ParkingSessionID'=> $_POST[ParkingSessionID],
			'RegisteredUserID' => $_POST[RegisteredUserID],  
			'LicensePlate' => $_POST[LicensePlate],   
			'TimeAmount'=> $_POST[TimeAmount],   
			'DeploymentID'=> $_POST[DeploymentID]
	);
        break; 		
     case 24:
	$fn="getParkingByID";
	$jsonData = array(
			'P_ID' =>  $_POST[ParkingID]
	);
        break; 	
     case 25:
	$fn="AddBackEndUser";
	$jsonData = array(
			'BU_Login' =>  $_POST[BU_Login],  
			'BU_Password' =>  $_POST[BU_Password],   
			'BU_Rights'=>  $_POST[BU_Rights],
			'BU_Name' => $_POST[BU_Name],  
			'BU_DeploymentID' =>  $_POST[BU_DeploymentID]
	);
        break;	

     case 26:
	$fn="LoginRetailerUser";
	$jsonData = array(
			'Login' => $_POST[Login],  
			'Password' => $_POST[Password],   
			'DeploymentID'=> $_POST[DeploymentID]
	);
        break;		

     case 27:
	$fn="LoginBackEndUser";
	$jsonData = array(
			'Login' => $_POST[Login],  
			'Password' => $_POST[Password]
	);
        break;		
}
	
//url de la petición
$url = $_SESSION[URL_Operation].$fn;
echo "<br>".$url;
//creamos el json a partir de nuestro array
$jsonDataEncoded = "json=".json_encode($jsonData);
//inicializamos el objeto CUrl
$ch = curl_init($url);
//Indicamos que nuestra petición sera Post
//curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");   
curl_setopt($ch, CURLOPT_POST, 1);
//Adjuntamos el json a nuestra petición
curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
//Agregamos los encabezados del contenido
curl_setopt($ch, CURLOPT_HTTPHEADER,array( "Content-type: application/x-www-form-urlencoded", 
            "Accept: application/json" ));
//para que la peticion no imprima el resultado como un echo comun, y podamos manipularlo
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//Trabajamos la respuesta obtenida del la url
$result = curl_exec($ch);

if(curl_exec($ch) === false)
	{
		echo 'Curl error: ' . curl_error($ch);
	}
	else
	{
		echo  "<br>".'RESULT SIN PARSE JSON';
		echo "<br>".$result;
		echo  "<br>".'OPERACION COMPLETADA SIN ERRORES';
		$json=json_decode($result);
		echo "<br>".$jsonDataEncoded;
		echo "<br>"."Result: ".$json->result."<br>Message:".$json->msg;
		
	}
	//	curl_close($curl);



 

//echo $jsonDataEncoded;

  /*
  parckplaceinforequest
  checking
   		P_ID int Optional
		PS_ID int Optional
		RU_Email X@y.z Optional
		RU_ID int Optional
		RU_CarPlate string Optional
		DeploymentID string Optional

  checkout-pagolist offers
  reserva
  pagos
  reserva
  topup
  registro
  
  */
  
  

?>